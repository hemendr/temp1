'use strict';
//1.
var q = require('q');
//2. 
var http = require('https');
//3.
module.exports = 
{
    //4.
    getData: function (options) 
    {
       console.log("entry to getData");
        //5.
        var defer = q.defer();
        var request;
        if (!options)
        {
            //6.
            defer.reject('Please specify the url to receive data');
        } 
        else 
        {
            console.log("prior to  http request")
            //7.
            request = http.request(options, function (response) 
            {
                console.log("after  http request")

                response.setEncoding('utf-8');
                response.on('data', function (chunk) 
                {
                    emp += chunk;
                    console.log("\n module.js - response:" + chunk);
                });
                //8.
                response.on('end', function () 
                {
                    try 
                    {
                        var receivedJson = JSON.parse(emp);
                        console.log("\n*************");

                        defer.resolve(receivedJson);
                    } 
                    catch (error) 
                    {
                        //9.
                        defer.reject(error);
                    }
                });
            });
        }
        //10.
        request.end();
        //11.
        return defer.promise;
    }
};