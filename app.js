'use strict';

const circuitBreaker = require('opossum');
var callModule = require('./module');
const express = require('express');

const app = express();
var server_port = process.argv[2] || 3010;


app.get('/listUsers', function(req, res) 
{
    var serverDetails = 
    {
        host: 'nodejs-2-mynodejs2.apps.tjxocpnp.tjx.com',
        port: '443',
        path: '/listUsers',
        method: 'GET'
    };
    const options = 
    {
        timeout: 1000, 
        errorThresholdPercentage: 1, 
        resetTimeout: 30000
    };
    console.log('Start.');
    const circuit = circuitBreaker(callModule.getData, options);
    circuit.fire(serverDetails)
    .then((response) => console.log(JSON.stringify(response)))
    .catch((err)     => console.log("error~~"));
    
    circuit.fallback(() => Promise.resolve({
        error: 'Unread messages currently unavailable. Try again later'
    }));;
});

var server = app.listen(server_port, function () 
{
	  var host = server.address().address;
	  var port = server.address().port;
	  console.log("app listening at http://%s:%s", host, port);
});

//server = app.listen(server_port);
console.log('Server running at http://0.0.0.0:'+ server_port);